#include <bits/stdc++.h>
using namespace std;

int sum_all(int num) {
    if (num/10 == 0) {
        return num;
    }

    return (num%10) + sum_all(num/10);
}

int main() {
    int num = 120345;
    cout <<sum_all(num) <<endl;
}
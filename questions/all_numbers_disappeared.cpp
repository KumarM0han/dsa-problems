class Solution {
public:
    vector<int> findDisappearedNumbers(vector<int>& nums) {
        int i=0;
        while (i < nums.size()){
            if (i==(nums[i]-1))
                i++;
            else if (nums[nums[i]-1] == nums[i])
                i++;
            else{
                int tmp = nums[nums[i]-1];
                nums[nums[i]-1] = nums[i];
                nums[i] = tmp;
            }
        }
        
        vector<int> res;
        for (i=0; i<nums.size(); i++){
            if (i != (nums[i]-1))
                res.push_back(i+1);
        }
        return res;
    }
};
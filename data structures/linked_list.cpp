#include <iostream>
#include <stdio.h>

using namespace std;

struct node{
    int val;
    struct node* next;
};
typedef struct node node_t;

void printlist(node_t *head){
    if (head == NULL){
        printf("EMPTY\n");
        return;
    }

    node_t *tmp = head;
    while( tmp != NULL){
        printf("%d -> ", tmp->val);
        tmp = tmp->next;
    }
    printf("NULL\n");
}

node_t *create_node(int elem){
    node_t *tmp = (node_t *)malloc(sizeof(node_t));
    tmp->val =elem;
    tmp->next =NULL;
    
    return tmp;
}

void insert_at_head(node_t **head, node_t *to_insert){
    to_insert->next =*head;
    *head =to_insert;
}

node_t *find_node(node_t *head, int key){
    node_t *tmp =head;
    while(tmp != NULL){
        if (tmp->val == key)
            return tmp;
        tmp = tmp->next;
    }
    return tmp;
}

int insert_after_node(node_t *insert_after, node_t *to_insert){
    if (insert_after == NULL){
        printf("Node doesn't exist\n");
        return -1;
    }
    to_insert->next = insert_after->next;
    insert_after->next =to_insert;

    return 0;
}

void insert_at_end(node_t *head, node_t *to_insert){
    node_t *tmp = head;
    while (tmp->next != NULL){
        tmp = tmp->next;
    }
    tmp->next = to_insert;
}

void clearlist(node_t *head){
    if (head != NULL){
        while (head != NULL){
            node_t *tmp = head->next;
            free(head);
            head =tmp;
        }
    }
}

int main(int argc, char const *argv[])
{
    node_t *head=NULL;
    for(int i=0; i<10; i++){
        node_t *tmp =create_node(i);
        insert_at_head(&head, tmp);
    }
    printlist(head);
    clearlist(head);

    return 0;
}

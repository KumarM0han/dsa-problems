#include <bits/stdc++.h>
using namespace std;

int find_len(char *s, int index=0) {
    if ( s[index] == '\0' ) {
        return index;
    }

    return find_len(s, index+1);
}

int main() {
    char s[] = "Mohan";
    cout <<find_len(s) <<endl;
}
#include <iostream>

class Node{
    public:
        int num;
        Node* next;
};

class LL{
    private:
        Node* head;
    public:
        LL(){
            head =nullptr;
        }
        ~LL(){
            while (head != nullptr){
                Node* tmp = head;
                head = head->next;
                delete tmp;
            }
            std::cout<<"CLEARED"<<std::endl;
        }

        void insert_at_head(int data){
            Node* tmp = new Node();
            tmp->num = data;
            tmp->next = head;
            head = tmp;
        }
        void print_list(){
            Node* tmp = head;
            while(tmp != nullptr){
                std::cout<<tmp->num<<" ";
                tmp =tmp->next;
            }
            std::cout<<std::endl;
        }
};

int main(){
    LL new_ll = LL();
    new_ll.insert_at_head(5);
    new_ll.insert_at_head(6);
    new_ll.insert_at_head(7);
    new_ll.print_list();
}
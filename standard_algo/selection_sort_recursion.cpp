#include <bits/stdc++.h>
using namespace std;

#define FOR(n,a) for(auto n : a)

void selection(vector<int>& arr, int r, int c, int index){
    if (r==0){
        return;
    }

    if (c < r){
        if (arr[c] > arr[index]){
            selection(arr, r, c+1, c);
        }else{
            selection(arr, r, c+1, index);
        }
    }else{
        int tmp = arr[c-1];
        arr[c-1] = arr[index];
        arr[index] = tmp;
        selection(arr, r-1, 0, 0);
    }
}

int main(){
    freopen("input.txt", "r", stdin);
    vector<int> v;
    int x;
    while(cin>>x){
        v.push_back(x);
    }
    FOR(i, v){
        cout<<i<<" ";
    }
    cout<<endl;

    selection(v, v.size(), 0, 0);
    
    FOR(i,v){
        cout<<i<<" ";
    }
    cout<<endl;
    fclose(stdin);
}
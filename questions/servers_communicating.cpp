class Solution {
public:
    int countServers(vector<vector<int>>& grid) {
        int ones=0;
        int rows = grid.size();
        int cols = grid[0].size();
        vector<int> rs(rows);
        vector<int> cs(cols);
        
        for (int i=0; i<rows; i++) {
            for (int j=0; j<cols; j++) {
                if (grid[i][j] == 1) {
                    rs[i]++;cs[j]++;
                }
            }
        }
        for (int i=0; i<rows; i++) {
            for (int j=0; j<cols; j++) {
                if (grid[i][j] && ( rs[i] > 1 || cs[j] > 1 )) {
                    ones++;
                }
            }
        }
        return ones;
    }
};
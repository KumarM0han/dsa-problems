#include <stdio.h>
#include <stdlib.h>
// time- O(nlgn)
// space- O(n)
int cmp(const void * a, const void * b){
    int *x = (int* )a;
    int *y = (int* )b;
    return *x-*y;
}
int heightChecker(int* heights, int heightsSize){
    int tmp[heightsSize];
    for(int i=0; i<heightsSize; i++){
        tmp[i]=heights[i];
    }
    qsort(tmp, heightsSize, sizeof(int), cmp);
    int wrong=0;
    for(int i=0; i<heightsSize; i++){
        if (tmp[i]!=heights[i])
            wrong++;
    }

    return wrong;
}
// time- n
// space- n
int another(int *heights, int heightsSize){
    int helper[heightsSize];
    for (int i=0; i<heightsSize; i++)
        helper[heights[i]]++;

    int count = 0, h = 0;
    for (int i = 0; h < heightsSize; i++) {
        while (helper[i]-- > 0) {
            if (heights[h] != i)
                count++;
            h++;
        }
    }

    return count;
}

//  * Definition for singly-linked list.
struct ListNode {
    int val;
    struct ListNode *next;
};

#include <stdlib.h>

typedef struct ListNode node;

struct ListNode* deleteDuplicates(struct ListNode* head){
    node* tmp=head;
    while((tmp!=NULL)&&(tmp->next!=NULL)){
        if(tmp->val == tmp->next->val)
            tmp->next =tmp->next->next;
        else{
            tmp=tmp->next;
        }
    }
    return head;
}
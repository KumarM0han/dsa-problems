#include <iostream>
#include <vector>

using namespace std;

void bubble(vector<int>& array, int r, int c){
    if (r==0){
        return;
    }
    if (c < r){
        if (array[c] > array[c+1]){
            int tmp =array[c];
            array[c] =array[c+1];
            array[c+1] =tmp;
        }
        bubble(array, r, c+1);
    }else{
        bubble(array, r-1, 0);
    }
}

int main(){
    freopen("input.txt", "r", stdin);
    vector<int> arr;
    int x;

    while(cin>>x){
        arr.push_back(x);
    }
    for(auto a: arr)
        cout<<a<<" ";
    cout<<endl;

    bubble(arr, arr.size()-1, 0);

    for(auto a: arr)
        cout<<a<<" ";
    cout<<endl;
}
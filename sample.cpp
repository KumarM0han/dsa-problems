#include <iostream>
#include <vector>
#include <unordered_map>
#include <queue>
using namespace std;

int main() {
    priority_queue<int, vector<int>, greater<int>> pq;
    pq.push(5);
    pq.push(3);
    pq.push(9);
    while (!pq.empty()) {
        cout <<pq.top()<<endl;
        pq.pop();
    }
}
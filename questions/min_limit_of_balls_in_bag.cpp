// LC 1760

#include <iostream>
#include <vector>
using namespace std;

int opr(vector<int>& v, int m, int mx) {
    int op = 0;
    for (auto n:v)
        op += (ceil((double)n/m)-1);
    return op<=mx;
}

class Solution {
public:
    int minimumSize(vector<int>& nums, int maxOperations) {
        int l=1, h= *max_element(nums.begin(), nums.end());
        int m;
        while (l<h) {
            m = l+(h-l)/2;
            if (opr(nums,m,maxOperations))
                h=m;
            else 
                l=m+1;
        }
        return l;
    }
};
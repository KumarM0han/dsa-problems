#include <bits/stdc++.h>
using namespace std;

void print_paths(vector<vector<bool>>& board, vector<vector<int>>& path, int r = 0, int c = 0, string p = {} , int step = 0 );

int main() {
    vector<vector<bool>> board{
        {true, true, true, true},
        {true, false, true, false},
        {true, true, false, true},
    };
    vector<vector<int>> path(board.size(), vector<int> (board[0].size()));
    print_paths(board, path);
}

void print_paths(vector<vector<bool>>& board, vector<vector<int>>& path, int r, int c, string p, int step) {
    if (r==board.size()-1 && c==board[0].size()-1) {
        path[r][c] = step+1;
        for (auto v: path) {
            cout <<"[ ";
            for (auto i: v) {
                cout <<i <<" ";
            }
            cout <<"]" <<endl;
        }
        for (auto c: p) 
            cout <<c;
        cout <<endl;
        return;
    }

    if (board[r][c] == false) {
        return;
    }
    board[r][c] = false;
    path[r][c] = step+1;

    if (r > 0) {
        string tmp = p;
        tmp += 'U';
        print_paths(board, path, r-1, c, tmp, step+1);
    }
    if (r < board.size()-1) {
        string tmp = p;
        tmp += 'D';
        print_paths(board, path, r+1, c, tmp, step+1);
    }
    if (c > 0) {
        string tmp = p;
        tmp += 'L';
        print_paths(board, path, r, c-1, tmp, step+1);
    }
    if (c < board[0].size()-1) {
        string tmp = p;
        tmp += 'R';
        print_paths(board, path, r, c+1, tmp, step+1);
    }

    path[r][c] = 0;
    board[r][c] = true;
}
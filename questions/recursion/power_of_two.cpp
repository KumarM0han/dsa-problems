#include <bits/stdc++.h>
using namespace std;

bool check(int num, int exp=1) {
    if (num == exp) {
        return true;
    }

    if (exp > num) {
        return false;
    }
    
    return check(num, exp*2);
}

int main() {
    int num;
    cout <<"Enter num: " <<endl;
    cin >>num;
    
    cout <<check(num) <<endl;
}
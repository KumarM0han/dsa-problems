#include <bits/stdc++.h>
using namespace std;

void rem_str(string& s, string& r, string& res, size_t index=0) {
    if (index == s.size()) {
        return;
    }
    string tmp = s.substr(index, r.size());
    if ( tmp.compare(r) == 0 ) {
        rem_str(s, r, res, index+r.size());
    } else {
        res += s.at(index);
        rem_str(s, r, res, index+1);
    }
}

int main() {
    string str;
    string rem;
    cout <<"Enter str" <<endl;
    getline(cin, str);
    cout <<"Enter rem" <<endl;
    getline(cin, rem);

    string res;
    rem_str(str, rem, res);
    for (auto c: res) {
        cout <<c;
    }
    cout <<endl;
}
// LC 875 https://leetcode.com/problems/koko-eating-bananas/
#include <bits/stdc++>
using namespace std;

#define ul long
int check(vector<int>& v, ul k){
    int h=0;
    for (int i=0;i <v.size(); i++) {
        int n=v[i];
        h += ((n/k)+ ( (n%k==0)?0:1 ) );
    }
    return h;
}

class Solution {
public:
    int minEatingSpeed(vector<int>& piles, int h) {
        ul l=1, hi=0;
        for(auto n:piles)
            hi+=n;
        ul long m;
        ul total =hi;
        while (l < hi) {
            m = l+(hi-l)/2;
            if (check(piles, m) <= h ){
                hi =m;
            } else {
                l =m+1;
            }
        }
        return hi;
    }
};
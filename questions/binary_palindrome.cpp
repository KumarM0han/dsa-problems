#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

void bpalin(unsigned int n)
{   
    unsigned int left=0;
    unsigned int right =0;
    int operations=0;
    
    for (int i=31; i>=0; i--){
        if ( (n&(1<<i))){
            left =i;
            break;
        }
    }
    while ( left > right ){
        bool l =(n&(1<<left));
        bool r =(n&(1<<right));

        if ((l^r)){
            if (l){
                operations += (1<<right);
            }else{
                operations -= (1<<right);
            }
        }
        right++;
        left--;
    }
    cout <<abs(operations)<<endl;
}

int main() {
    unsigned int T;cin>>T;
    vector<unsigned int> ns;
    unsigned int x;
    for (unsigned int i=0; i<T; i++){
        cin>>x;
        ns.push_back(x);
    }
    for(auto i: ns){
        bpalin(i);
    }
}

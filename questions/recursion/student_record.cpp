#include <bits/stdc++.h>
using namespace std;

bool check(string& s, int index=0, int countA=0, int countL=0) {
    if ( (countA > 1) || (countL > 2) ) {
        return false;
    }

    if (index == s.size()) {
        return true;
    }

    switch (s.at(index)) {
        case 'P' : return check(s, index+1, countA, 0);
        case 'A' : return check(s, index+1, countA+1, 0);
        case 'L' : return check(s, index+1, countA, countL+1);
    }
    return true;
}

int main() {
    string record;
    cout <<"Enter record" <<endl;
    getline(cin, record);

    cout <<check(record) <<endl;
}
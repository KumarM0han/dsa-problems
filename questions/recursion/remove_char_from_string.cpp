#include <bits/stdc++.h>
using namespace std;

string rm_dup(string& s, char t, int index=0) {
    string tmp;
    if (index == s.size()) {
        tmp = "";
        return tmp;
    }

    if (s.at(index) != t) {
        tmp += s.at(index);
        tmp += rm_dup(s, t, index+1);
        return tmp;
    }
    return rm_dup(s, t, index+1);
}

int main() {
    string str;
    cout <<"Enter str" <<endl;
    getline(cin, str);
    char ch;
    cout <<"Enter char " <<endl;
    cin >>ch;

    string res = rm_dup(str, ch);
    for (auto c: res) {
        cout <<c <<" ";
    }
    cout <<endl;
}
#include <bits/stdc++.h>
using namespace std;

int target_sum(vector<int>& a, int target, size_t index = 0, int sum = 0);

int main() {
    vector<int> a(5, 1);
    int target = 3;
    int ways = target_sum(a, target);
    cout <<ways <<endl;
}

int target_sum(vector<int>& a, int target, size_t index, int sum) {
    if (index == a.size()) {
        if (sum == target) 
            return 1;
        return 0;
    }

    return target_sum(a, target, index+1, sum + a.at(index)) +  target_sum(a, target, index+1, sum - a.at(index));
}
#include <bits/stdc++.h>
using namespace std;

int way_count(int r, int c);
void print_path(int r, int c, string p = {});

int main() {
    int r, c;
    cout <<"Enter r&c" <<endl;
    cin >>r >>c;
    
    print_path(r, c);
}

void print_path(int r, int c, string p) {
    if (r==1 && c==1) {
        for (auto c: p)
            cout <<c;
        cout <<endl;
        return;
    }
    if (r > 1) {
        string t = p;
        t += 'R';
        print_path(r-1, c, t);
    }
    if (c > 1) {
        string t = p;
        t += 'D';
        print_path(r, c-1, t);
    }
}

int way_count(int r, int c) {
    if (r==1 || c==1) {
        return 1;
    }
    return way_count(r-1, c) + way_count(r, c-1);
}
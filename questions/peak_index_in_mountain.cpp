//Linear O(n)
class Solution {
public:
    int peakIndexInMountainArray(vector<int>& arr) {
        for (int i=1; i<arr.size()-1; i++){
            if (arr[i]>arr[i+1] && arr[i]>arr[i-1])
                return i;
        }
        return -1;
    }
};
// Binary O(log n)
class Solution {
public:
    int peakIndexInMountainArray(vector<int>& arr) {
        int a=0;
        int b=arr.size()-1;
        int mid;
        
        while(a<b){
            mid = a+(b-a)/2;
            if (arr[mid]<arr[mid+1])
                a=mid+1;
            else
                b=mid;
        }
        return a;
    }
};
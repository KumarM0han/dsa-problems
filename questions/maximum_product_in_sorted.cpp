#include <iostream>
#include <vector>
#include <stdio.h>
#include <limits.h>
#include <algorithm>
#include <iterator>
#include <math.h>

int main()
{
    std::vector<int> nums{-4,-1,0};
    std::sort(nums.begin(), nums.end());
    int prod=0;
    
    prod = nums.at(0)*nums.at(1);
    prod = std::max(nums.at(nums.size()-1)*nums.at(nums.size()-2), prod);
    
    printf("%d\n", prod);
}

#include <bits/stdc++.h>
using namespace std;

vector<int> merge_return(vector<int>& arr);
vector<int> merge_both(vector<int>& left, vector<int>& right);

int main(){
    vector<int> vi{5,4,3,2,1};
    
    for (auto n: vi)
        cout<<n<<" ";
    cout<<endl;
    
    vector<int> sorted = merge_return(vi);
    for(auto n:sorted)
        cout<<n<<" ";

    cout<<endl;
}

vector<int> merge_return(vector<int>& arr){
    if (arr.size() < 2) {
        return arr;
    }

    size_t mid = arr.size()/2;
    vector<int> left(mid);
    vector<int> right(arr.size()-mid);

    for(size_t i=0; i<mid; i++) {
        left.at(i) = arr.at(i);
    }
    for (size_t i=0; i<(arr.size()-mid); i++) {
        right.at(i) = arr.at(mid+i);
    }
    
    vector<int> left_sorted;
    vector<int> right_sorted;

    left_sorted = merge_return(left);
    right_sorted = merge_return(right);

    return merge_both(left_sorted, right_sorted);
}

vector<int> merge_both(vector<int>& left, vector<int>& right) {
    vector<int> result(left.size() + right.size());
    size_t li =0, ri=0, k =0;

    while (li < left.size() && ri < right.size()) {
        if (left.at(li) < right.at(ri)) {
            result.at(k++) = left.at(li++);
        } else {
            result.at(k++) = right.at(ri++);
        }
    }
    while (li < left.size()) {
        result.at(k++) = left.at(li++);
    }
    while (ri < right.size()) {
        result.at(k++) = right.at(ri++);
    }

    return result;
}
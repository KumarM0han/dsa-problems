#include <bits/stdc++.h>
using namespace std;

int find_peaks(vector<int>& a, int find_max=1, int index=0) {
    if (index == a.size()-1) {
        return a.at(0);    
    }

    if (find_max) {
        return max(a.at(index), find_peaks(a, find_max, index+1));
    } else {
        return min(a.at(index), find_peaks(a, find_max, index+1));
    }
}

int main() {
    vector<int> a{1,4,3,-5,-4,8,6};
    int high = find_peaks(a);
    int low = find_peaks(a, 0);

    cout <<"Max: " <<high <<endl;
    cout <<"Min: " <<low <<endl;
}
#include <stdio.h>
#include <stdlib.h>


int cycle_exist(node* head){
    if(head==NULL || head->next==NULL)
        return 0;
    
    node* slow=head;
    node* fast=head->next;
    while(slow!=fast){
        if(fast==NULL || fast->next==NULL)
            return 0;
        slow =slow->next;
        fast =fast->next->next;
    }
    return 1;
}
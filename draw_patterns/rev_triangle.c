#include <stdio.h>
#include <stdlib.h>

int main(){
    int h;
    printf("height\n");
    scanf("%d", &h);

    for(int i=0; i<h; i++){
        for(int j=h-i; j>0; j--){
            printf("*");
        }
        printf("\n");
    }
}
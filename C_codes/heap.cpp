#include <iostream>
#include <vector>
using namespace std;

class Heap 
{
private:
    vector<int> heap_v;
    size_t heap_size;
public:

    Heap(vector<int> vec){
        heap_v = vec;
        heap_size = heap_v.size();
        cout <<"Building" <<endl;
        build_max_heap();
        cout <<"Done" <<endl <<endl <<endl;
    }

    void print();
    void max_heapify(size_t ind);
    void build_max_heap();
    void sort_heap();
};

void Heap::max_heapify(size_t ind) {
    if (ind == heap_size)
        return;
    
    size_t l = 2*ind + 1;
    size_t r = 2*ind + 2;
    size_t largest;
    if (l < heap_size && heap_v.at(l) > heap_v.at(ind))
        largest = l;
    else
        largest = ind;
    
    if (r < heap_size && heap_v.at(r) > heap_v.at(largest))
        largest = r;
    
    if (ind != largest) {
        int tmp = heap_v.at(largest);
        heap_v.at(largest) = heap_v.at(ind);
        heap_v.at(ind) = tmp;

        max_heapify(largest);
    }
}

void Heap::build_max_heap() {
    int last_node = (int)heap_size/2;
    for(int i=last_node; i >= 0; i--) {
        cout <<"Heapify: " <<i <<endl;
        max_heapify(i);
    }
}

void Heap::print() {
    for (size_t i=0; i < heap_size; i++) {
        cout <<heap_v.at(i) <<" ";
    }
    cout <<endl;
}

void Heap::sort_heap() {
    cout <<endl <<"Sorting" <<endl;
    for(int iter = (int)heap_v.size()-1; iter > 0; iter--) {
        printf("swapped %d->%d to %d->%d\n", iter, heap_v.at(iter), 0, heap_v.at(0));
        
        int tmp = heap_v.at(iter);
        heap_v.at(iter) = heap_v.at(0);
        heap_v.at(0) = tmp;

        heap_size--;

        max_heapify(0);
    }
}


int main() {
    vector<int> A = {5,13,2,25,7,17,20,8,4};
    Heap h(A);
    h.print();
    h.sort_heap();
    h.print();
}
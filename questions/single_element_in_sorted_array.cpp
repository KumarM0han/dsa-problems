// Time- lg n
// Space- 1
class Solution {
public:
    int singleNonDuplicate(vector<int>& nums) {
        int low=0, high=nums.size()-1;
        int mid;
        while(low<high){
            mid =low+(high-low)/2;
            if (mid%2==0)
                mid--;
            if (nums[mid] ==nums[mid+1])
                high =mid-1;
            else
                low =mid+1;
        }
        return nums[low];
    }
};
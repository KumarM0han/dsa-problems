#include <bits/stdc++.h>
using namespace std;

string remove_dups(string& s, int uniq=0, int ahead=1) {
    string tmp;
    tmp = s.at(uniq);
    if (ahead == s.size()) {
        return tmp;
    }

    if (s.at(ahead) == s.at(uniq)) {
        return remove_dups(s, uniq, ahead+1);
    } else {
        tmp += remove_dups(s, ahead, ahead+1);
        return tmp;
    }
}

int main() {
    string str;
    cout <<"Enter str: " <<endl;
    getline(cin, str);
    string filtered = remove_dups(str);
    cout <<filtered <<endl;
}
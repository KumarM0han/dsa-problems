class Solution {
public:
    int firstMissingPositive(vector<int>& nums) {
        int i=0;
        while (i<nums.size()){
            if ((nums[i]<=0) || (nums[i]>nums.size()) || (nums[nums[i]-1] == nums[i]) || (i==(nums[i]-1)))
                i++;
            else{
                int tmp = nums[nums[i]-1];
                nums[nums[i]-1] = nums[i];
                nums[i] = tmp;
            }
        }
        for (i=0; i<nums.size(); i++){
            if (i != nums[i]-1)
                return i+1;
        }
        return nums.size()+1;
    }
};
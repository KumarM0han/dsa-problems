#include <bits/stdc++.h>
using namespace std;

int find_steps(int num, int steps=0) {
    if (num == 0) {
        return steps;
    }

    steps++;
    if (num%2 == 0) {
        return find_steps(num/2, steps);
    } else {
        return find_steps(num-1, steps);
    }

    return -1;
}

int main() {
    int num;
    cout <<"Enter num: " <<endl;
    cin >>num;
    int steps = find_steps(num);
    cout <<steps <<endl;
}
#include <bits/stdc++.h>
using namespace std;

int specialArray(vector<int>& nums) {
    sort(nums.begin(), nums.end());
    int mid;
    if (nums.size()%2 == 0)
        mid = (nums.size()/2)-1;
    else
        mid = nums.size()/2;
    
    int count = nums.size()-mid;
    int low=0, high=mid-1;
    int center;
    while(low<=high){
        center = low+(high-low)/2;
        if (nums[center] >= count)
            return -1;
        if (nums[center] < count){
            low =center+1;
        }
    }
    for (size_t i=mid; i<nums.size(); i++) {
        cout<< nums.at(i)<< " "<<count<<" "<< (nums.at(i) < count)<<endl;
        if (nums.at(i) < count) {
            return -1;
        }
    }
    return count;
}

int main(){
    vector<int> v{3,3,6,6,7,8,8,9};
    int re = specialArray(v);
    cout<<re<<endl;
}
#include <bits/stdc++.h>
using namespace std;

void print_permute(string& s, string p = {}, size_t index=0);
vector<string> return_permute(string& s, string p = {}, size_t index = 0);

int main() {
    string str;
    cout <<"Enter str" <<endl;
    getline(cin, str);

    vector<string> perm = return_permute(str);
    for (auto s: perm) {
        cout <<s <<endl;
    }
}

vector<string> return_permute(string& s, string p, size_t index) {
    vector<string> hold;
    if (p.size() == s.size()) {
        hold.push_back(p);
        return hold;
    }
    char ch = s.at(index);

    for (auto i=0; i<=p.size(); i++) {
        string tmp, slice;
        
        slice = p.substr(0, i);
        tmp += slice;

        tmp += ch;

        slice = p.substr(i, p.size());
        tmp += slice;

        vector<string> lower = return_permute(s, tmp, index+1);
        for (auto s: lower) {
            hold.push_back(s);
        }
    }
    return hold;
}

void print_permute(string& s, string p, size_t index ) {
    if(p.size() == s. size()) {
        for (auto c: p)
            cout <<c;
        cout <<endl;
        return;
    }

    char ch = s.at(index);

    for (size_t i=0; i<p.size()+1; i++) {
        string tmp;
        string slice;
        
        slice = p.substr(0, i);
        tmp += slice;

        tmp += ch;
        
        slice = p.substr(i, p.size());
        tmp += slice;
        
        print_permute(s, tmp, index+1);
    }
}
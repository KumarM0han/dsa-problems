// package questions;
import java.util.Arrays;
import java.util.Comparator;
public class compareStringByFrequency {
    public static void main(String[] args) {
        String[] s = { "cbd", "zaaz", "aaabc" };
        // int[] a = {5,6,8,3,1,5,9,3};
        Arrays.sort(s, new Comparator<String>() {
            @Override
            public int compare(String a, String b) {
                return mostFreq(a) - mostFreq(b);
            }
        });
        System.out.println(Arrays.toString(s));
    }
    static int mostFreq(String str) {
        int count = 1;
        char smallest = 'z'+1;
        for (char ch : str.toCharArray()) {
            if ( ch < smallest ) {
                count = 1; smallest = ch;
            } else if(ch == smallest) {
                count++;
            }
        }
        return count;
    }
}
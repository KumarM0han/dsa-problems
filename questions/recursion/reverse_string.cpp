class Solution {
public:
    void reverseString(vector<char>& s, int done=0) {
        if (done >= s.size()-1-done) {
            return;
        }
        
        char tmp = s.at(s.size()-1-done);
        s.at(s.size()-1-done) = s.at(done);
        s.at(done) = tmp;
        
        reverseString(s, done+1);
    }
};
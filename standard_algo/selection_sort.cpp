#include <iostream>
#include <vector>
#include <stdio.h>
#include <limits.h>
#include <algorithm>

using namespace std;

void my_sort(vector<int>& list){

	for (long unsigned int i=0; i<list.size(); i++){
		int high = 0;
		for (long unsigned int j=1; j<list.size()-1-i; j++){
			if (list[j] > list[high]){
				high = j;
			}
		}
		int tmp = list[list.size()-1-i];
		list[list.size()-1-i] = list[high];
		list[high] = tmp;
	}
}

int main(int argc, char const *argv[])
{
	freopen("input.txt", "r", stdin);
	freopen("output.txt", "w", stdout);

	vector<int> arr;
	int x;
	while(cin >> x){
		arr.push_back(x);
	}

	my_sort(arr);

	for (auto i : arr)
		fprintf(stdout, "%d ", i);
	
	cout << endl;
	fclose(stdin);
	fclose(stdout);
	return 0;
}
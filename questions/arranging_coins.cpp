class Solution {
public:
    int arrangeCoins(int n) {
        unsigned long a=1,b=n, ans;
        unsigned long int mid;
        
        while(a<=b){
            mid = (a+b)/2;
            unsigned long int sum = (mid*(mid+1))/2;
            if (sum == n)
                return mid;
            else if (sum < n) {ans=mid;a=mid+1;}
            else b=mid-1;
        }
        return ans;
    }
};
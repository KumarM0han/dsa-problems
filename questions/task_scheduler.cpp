class Solution {
public:
    int max(int a, int b) {
        return (a < b) ? b : a;
    }
    
    int leastInterval(vector<char>& tasks, int n) {
        vector<int> freq(26);
        int high=0;
        int repeats=0;
        
        for (auto c: tasks) {
            freq.at(c - 'A')++;
            if (freq.at(c - 'A') > high) {
                repeats = 1;
                high = max(high, freq.at(c - 'A'));
            } else if (freq.at(c - 'A') == high) {
                repeats++;
            }
        }
        
        int rect = (high-1)*(n+1);
        return max(rect+repeats, tasks.size());
    }
};
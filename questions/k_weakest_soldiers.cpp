#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;
/*
[[1,1,0,0,0],
 [1,1,1,1,0],
 [1,0,0,0,0],
 [1,1,0,0,0],
 [1,1,1,1,1]], 

0 1 2 3 4
2 4 1 2 5 ; k=3,2

2 0 3 1 5
1 2 2 4 5
 
*/
// k smallest num
vector<int> k_smallest(vector<int>& v, int k);

int main() {
	vector<int> a{1,2,2,2};
	int k=3;
	vector<int> r = k_smallest(a, k);
	for(auto i: r) {
		cout <<i <<" ";
	}
	cout <<endl;
}

vector<int> k_smallest(vector<int>& v, int k) {
	vector<int> res = v;
	sort(res.begin(), res.end());
	res.erase(res.begin()+k, res.end());
	return res;
}
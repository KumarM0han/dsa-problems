#include <bits/stdc++.h>
using namespace std;

bool check_sorted(vector<int>& a, size_t index=0) {
    if (index == a.size()-1) {
        return true;
    }

    return ( a.at(index) <= a.at(index+1) ) && check_sorted(a, ++index);
}

int main() {
    vector<int> ar{1,2,3,4,5,6,2};
    
    cout <<check_sorted(ar) <<endl;
}
#include <iostream>
#include <vector>
#include <stdio.h>
#include <limits.h>
#include <algorithm>

using namespace std;

void my_sort(vector<int>& ar){
    for (size_t i=0; i<ar.size()-1; i++){
        int j=i+1;
        while (ar[j] < ar[j-1]){
            int tmp = ar[j-1];
            ar[j-1] = ar[j];
            ar[j] = tmp;
            --j;
        }
    }
}

int main()
{
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    vector<int> arr;
    int x;
    while (cin >> x){
        arr.push_back(x);
    }

    my_sort(arr);

    for (auto i : arr)
        fprintf(stdout, "%d ", i);

    cout << endl;
    fclose(stdin);
    fclose(stdout);
    return 0;
}

#include <bits/stdc++.h>
using namespace std;

void first_caps(string& s, int index=0) {
    if (index == s.size()) {
        cout <<"No Caps!" <<endl;
        return;
    }
    if (isupper(s.at(index))) {
        cout <<s.at(index) <<endl;
        return;
    }

    first_caps(s, index+1);
}

int main() {
    string str = "Kumaroohan";
    first_caps(str);   
}
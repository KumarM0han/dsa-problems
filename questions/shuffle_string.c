#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * restoreString(char * s, int* indices, int indicesSize);

int main(){
    char *s ="codeleet";
    int n[] ={4,5,6,7,0,2,1,3};
    int size =sizeof(n)/sizeof(n[0]);
    printf("called\n");

    char *res = restoreString(s, n, size);
    printf("%s\n", res);
}

char * restoreString(char * so, int* indices, int indicesSize){
    char *s =malloc(strlen(so)+1);
    strcpy(s, so);
    int i=0;
    while (i<indicesSize){
        if (i != indices[i]){
            char t =s[i];
            s[i] =s[indices[i]];
            s[indices[i]] =t;
            int tmp =indices[i];
            indices[i] =indices[indices[i]];
            indices[tmp] =tmp;
        }else{
            i++;
        }
    }
    return s;
}
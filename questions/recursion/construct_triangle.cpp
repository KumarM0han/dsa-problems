#include <bits/stdc++.h>
using namespace std;

void construct_rect(int area, vector<int>& store, int i=1) {
    if (i*i > area) {
        return;
    }
    int j = area/i;
    if (i*j == area) {
        int L = max(i, j);
        int W = min(i, j);
        
        if ( (store.at(0)==0 && store.at(1)==0) || ((L-W) < (store.at(0)-store.at(1))) ) {
            store.at(0) = L;
            store.at(1) = W;
            construct_rect(area, store, i+1);
        }
    }
    construct_rect(area, store, i+1);
}

int main() {
    int num;
    cout <<"Enter num: " <<endl;
    cin >>num;

    vector<int> l_w(2);
    construct_rect(num, l_w);
    for (auto n: l_w) {
        cout <<n <<" ";
    }
    cout <<endl;
}
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

char * defangIPaddr(char * address);

int main(){
    char *ip = "255.100.50.0";
    char *def_ip = defangIPaddr(ip);
    printf("%s\n", def_ip);
    free(def_ip);
}

char * defangIPaddr(char * address){
    char *new = malloc(strlen(address)+1+6);
    size_t size = strlen(address);
    int seen =0;
    
    for(size_t i=0; i<size; i++){
        if (address[i] == '.'){
            new[i+2*seen] = '[';
            new[i+1+2*seen] ='.';
            new[i+2+2*seen] =']';
            seen++;
        }else{
            new[i+seen*2] = address[i];
        }
    }
    new[size+seen*2] = '\0';
    return new;
}
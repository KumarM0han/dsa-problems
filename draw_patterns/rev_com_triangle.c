#include <stdio.h>
#include <stdlib.h>

int main(){
    int h;
    printf("height\n");
    scanf("%d", &h);

    for(int i=h-1; i>=0; i--){
        for(int s=0; s<h-1-i; s++){
            printf(" ");
        }
        for(int s=0; s<i+1; s++){
            printf("*");
        }
        for(int s=1; s<i+1; s++){
            printf("*");
        }
        printf("\n");
    } 
}
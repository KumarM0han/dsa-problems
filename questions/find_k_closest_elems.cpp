#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    vector<int> a = {1,3,5,6,9,11,13,17};
    int target;
    cout <<"Find what? "; cin >>target;
    vector<int>::iterator lb, ub;
    lb = lower_bound(a.begin(), a.end(), target);
    ub = upper_bound(a.begin(), a.end(), target);

    if (lb == ub) {
        cout <<target <<" doesn't exist." <<endl;
    } else {
        cout <<target <<" found at: " <<lb-a.begin() <<endl;
    }
}
#include <bits/stdc++.h>
using namespace std;

void subseq(string& str, string p = {}, size_t index=0) {
    if (index == str.size()) {
        for (auto c: p)
            cout <<c;
        cout <<endl;
        return;
    }
    char ch = str[index];

    string pt = p;
    p += ch;
    subseq(str, p, index+1);
    subseq(str, pt, index+1);
}

vector<string> return_subseq(string& str, string p = {}, size_t index=0) {
    vector<string> hold;
    if (index == str.size()) {
        hold.push_back(p);
        return hold;
    }
    char ch = str.at(index);
    string tp = p;
    p += ch;
    
    vector<string> tmp = return_subseq(str, p, index+1);
    for (auto c: tmp) {
        hold.push_back(c);
    }
    
    tmp = return_subseq(str, tp, index+1);
    for (auto c: tmp) {
        hold.push_back(c);
    }
    return hold;
}

vector<vector<int>> iterative_subseq(vector<int>& a) {
    vector<vector<int>> outer;
    outer.push_back({});
    
    for(int n: a) {
        int sa = outer.size();
        for (auto i=0; i<sa; i++) {
            vector<int> inner = outer.at(i);
            inner.push_back(n);
            outer.push_back(inner);
        }
    }
    return outer;
}

int main() {
    vector<int> a{1,2,3};
    vector<vector<int>> res = iterative_subseq(a);
    for (auto v: res) {
        for (auto n: v) {
            cout <<n <<" ";
        }
        cout <<endl;
    }
}
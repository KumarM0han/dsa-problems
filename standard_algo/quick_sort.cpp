#include <bits/stdc++.h>
using namespace std;

void quick_sort(vector<int>& array, int low, int high) {
    if (low >= high) {
        return;
    }

    int start =low, end =high;
    int mid = low + (high - low) / 2;
    int pivot = array.at(mid);

    while (start <= end) {
        while (array[start] < pivot) {
            start++;
        }
        while (array[end] > pivot) {
            end--;
        }

        if (start <= end) {
            int tmp = array.at(start);
            array.at(start) = array.at(end);
            array.at(end) = tmp;
            start++;
            end--;
        }
    }
    quick_sort(array, low, end);
    quick_sort(array, start, high);
}

int main() {
    vector<int> arr{5,4,3,2,1};
    quick_sort(arr, 0, arr.size()-1);
    
    for (auto n: arr) {
        cout << n<< " ";
    }

    cout<<endl;
}

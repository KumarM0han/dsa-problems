#include <bits/stdc++.h>
using namespace std;

void print_paths(vector<vector<bool>> path, int r = 0, int c = 0, string p = {});

int main() {
    vector<vector<bool>> board{
        {true, true, true},
        {true, false, false},
        {true, false, true},
    };
    print_paths(board);
}

void print_paths(vector<vector<bool>> path, int r, int c, string p) {
    if ( (r==path.size()-1) && (c==path[0].size()-1) ) {
        for (auto c: p) 
            cout <<c;
        cout <<endl;
        return;
    }

    if (path[r][c] == false) {
        return;
    }
    
    if (r < (path.size()-1) ) {
        string tmp = p;
        tmp += 'D';
        print_paths(path, r+1, c, tmp);
    }
    if (c < (path[0].size()-1) ) {
        string tmp = p;
        tmp += 'R';
        print_paths(path, r, c+1, tmp);
    }
}
// C implementation. 
// Space-O(1) Time-O(N log N)
int cmp(const void * a, const void * b){
    int *x = (int *)a;
    int *y = (int *)b;
    return *x - *y;
}

bool checkIfExist(int* arr, int arrSize){
    qsort(arr, arrSize, sizeof(int), cmp);
    for (int i=0; i<arrSize; i++){
        if (arr[i]%2 != 0)
            continue;
        int ap = 0;
        int bp = arrSize-1;
        int mid;
        int elem=arr[i]/2;
        
        while (ap<bp){
            mid = (ap+bp)/2;
            if ((arr[mid] == elem) && (mid!=i)){
                return true;
            }
            else if (arr[mid]<elem)
                ap=mid+1;
            else
                bp=mid-1;
        }
        if ((arr[ap] == elem) && (ap!=i))
            return true;
    }
    return false;
}

// C++.
// Other's solution
// space - n
// time - n
// unordered_map
class Solution {
public:
    bool checkIfExist(vector<int>& arr) {
        unordered_map<int, int> mp;
               
        for(int i: arr){
            if((i%2 == 0 && mp[i/2]) or mp[2*i]){
                return true;
            }
            mp[i]++;
        }
       return false;        
    }
};
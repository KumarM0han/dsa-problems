// O(n)
class Solution {
public:
    int findKthPositive(vector<int>& arr, int k) {
        int to_remove=0;
        int i=0,j=0;
        while(i<arr.size()){
            if (j+1 != arr[i]){
                to_remove++;
                j++;
            }else{
                j++;
                i++;
            }
            if (to_remove==k)
                break;
        }
        if (to_remove < k)
            return arr[arr.size()-1]+k-to_remove;
        else
            return j;
    }
};

// Binary search O(log n)
class Solution {
public:
    int findKthPositive(vector<int>& arr, int k) {
        int last = arr.size()-1;
        int missing = arr[last]-arr.size();
        
        if (missing < k)
            return arr[last]+k-missing;
        else{
            int a=0;
            int b=last;
            int m;
            while (a<b){
                m = a+(b-a+1)/2;
                
                if (arr[m]-m-1 >= k)
                    b=m-1;
                else
                    a=m;
            }
            if (arr[a]-a-1 < k)
                return k+a+1;
            else
                return k;
        }
    }
};
#include <bits/stdc++.h>
using namespace std;

#define FOR(a,b) for(auto a:b)

void insertion(vector<int>& arr, int c, int index){
    if((size_t)c == arr.size()-1){
        return;
    }

    if (arr[index-1] > arr[index]){
        int tmp = arr[index-1];
        arr[index-1] = arr[index];
        arr[index] = tmp;
        insertion(arr, c, index-1);
    }else{
        insertion(arr, c+1, c+2);
    }
}

int main(){
    freopen("input.txt", "r", stdin);
    vector<int> vi;
    int x;
    while(cin>>x){
        vi.push_back(x);
    }
    FOR(i,vi){
        cout<<i<<" ";
    }cout<<endl;
    
    insertion(vi, 0, 1);
    
    FOR(i,vi){
        cout<<i<<" ";
    }cout<<endl;

    fclose(stdin);
}
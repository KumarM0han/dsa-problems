#include <bits/stdc++.h>
using namespace std;

void merge_both(vector<int>& left, vector<int>& right, vector<int>& result){
    size_t li =0, ri =0;
    size_t k=0;

    while (li<left.size() && ri<right.size()) {
        if (left[li] < right[ri]) {
            result[k++] = left[li++];
        } else {
            result[k++] = right[ri++];
        }
    }
    while (li < left.size()) {
        result[k++] = left[li++];
    }
    while (ri < right.size()) {
        result[k++] = right[ri++];
    }
}

void my_merge_sort(vector<int>& arr){
    if (arr.size() < 2)
        return;
    
    int mid = arr.size()/2;
    vector<int> left(mid);
    vector<int> right(arr.size()-mid);

    for (int i=0; i<mid; i++) {
        left.at(i) = arr.at(i);
    }
    for (int i=0; i< (int)(arr.size()-mid); i++) {
        right.at(i) = arr.at(mid+i);
    }

    my_merge_sort(left);
    my_merge_sort(right);

    merge_both(left, right, arr);
}

int main(){
    vector<int> vi{5,4,3,2,1};

    my_merge_sort(vi);
    for(auto n:vi)
        cout<<n<<" ";

    cout<<endl;
}


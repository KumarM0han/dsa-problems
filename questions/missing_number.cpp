class Solution {
public:
    int missingNumber(vector<int>& nums) {
        int i=0;
        while (i<nums.size()){
            if (i == nums[i])
                i++;
            else if(nums[i] > nums.size()-1)
                i++;
            else{
                int tmp = nums[nums[i]];
                nums[nums[i]] = nums[i];
                nums[i] = tmp;
            }
        }
        
        for (i=0; i<nums.size(); i++){
            if (i != nums[i])
                return i;
        }
        return nums.size();
    }
};
#include <bits/stdc++.h>
using namespace std; 

void sum_triangle(vector<int>& a, int start) { 
    if ((size_t)start == a.size()-1) {
        cout << a.at(start) <<endl;
        return;
    }

    int next = start+1;
    vector<int> copy;
    for (size_t i=next; i<a.size(); i++) {
        int sum = a.at(i) + a.at(i-1);
        copy.push_back(sum);
    }
    sum_triangle(copy, start);

    for (auto n:a) {
        cout << n <<" ";
    }
    cout <<endl;
}

int main() {
    vector<int> a{1,2,3,4,5};
    sum_triangle(a, 0);
}
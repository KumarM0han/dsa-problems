class Solution {
public:
    int searchInsert(vector<int>& nums, int target) {
        int lo=0;
        int hi=nums.size()-1;
        int mid;
        
        while (lo<hi){
            mid=lo+(hi-lo+1)/2;
            if (nums[mid] > target)
                hi=mid-1;
            else
                lo=mid;
        }
        if (nums[lo]==target || nums[lo]>target)
            return lo;
        
        return lo+1;
    }
};
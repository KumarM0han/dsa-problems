// saw this in errichto's videos
// time -lg n for sorting
// space- 1, cuz we are altering the array else this would have been O(n)
class Solution {
public:
    int majorityElement(vector<int>& nums) {
        sort(nums.begin(), nums.end());
        int x=nums.size()/2;
        return nums[x];
    }
};

/*
Assume first elem to be the majority elem.
everytime we see it we increment its strength else we decrement
if this is strength is 0 now, that  means we have seen exactly i/2 occurence of last guess,
we want more than half appearing number. 
so we assum curent n. to be majority and proceed similarly
*/
class Solution {
public:
    int majorityElement(vector<int>& nums) {
        int guess=nums[0];
        int iscand=1;
        if(nums.size()>1){
            for(int i=1; i<nums.size(); i++){
                if(nums[i]==guess){
                    iscand++;
                }else{
                    iscand--;
                }
                if (iscand==0){
                    guess=nums[i];
                    iscand=1;
                }
            }
        }else{
            return nums[0];
        }
        return guess;
    }
};
// This is O(m+n) not O(m*n)

int countNegatives(int** grid, int gridSize, int* gridColSize){
    int ir = gridSize-1;
    int ic = 0;
    int sum = 0;
    
    while (ir>=0 && ic<(*gridColSize)){
        if (grid[ir][ic]<0){
            sum += (*gridColSize-ic);
            ir--;
        }
        else{
            ic++;
        }
    }
    return sum;
}
// time- n
// space- 1
// alternate soln would be to sort and then square each, giving (n lgn) time 
#include <stdio.h>
#include <stdlib.h>

#define ABS(x) ((x<0)?(-x):(x))
int* sortedSquares(int* nums, int numsSize, int* returnSize);

int main()
{
    int arr[] = {-11,-1,0,3,10};
    int size= sizeof(arr)/sizeof(arr[0]);
    int returnSize;
    int *res = sortedSquares(arr, size, &returnSize);
    for(int i=0; i< returnSize; i++){
        printf("%d ", res[i]);
    }
    free(res);
    
    printf("\n");
    return 0;
}

int* sortedSquares(int* nums, int numsSize, int* returnSize){
    int *res = malloc(sizeof(int)*numsSize);
    *returnSize = numsSize;
    
    int start=0, end=numsSize-1;
    while(start<=end){
        size_t filled = start + (numsSize-end-1);
        if (abs(nums[end]) >= abs(nums[start])){
            res[numsSize -1 -filled] = nums[end]*nums[end];
            end--;
        }else{
            res[numsSize - 1 -filled] = nums[start]*nums[start];
            start++;
        }
    }
    return res;
}

#include <iostream>
#include <vector>
#include <stdio.h>
#include <limits.h>
#include <algorithm>

using namespace std;

void my_sort(vector<int>& ar){
    int i=0;
    while (i<ar.size()){
        if (i == (ar[i])-1)
            i++;
        else{
            int tmp = ar[ar[i]-1];
            ar[ar[i]-1] = ar[i];
            ar[i] = tmp;
        }
    }
}

int main()
{   
    freopen("intput.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
    vector<int> arr;
    int x;
    while(cin >> x){
        arr.push_back(x);
    }

    my_sort(arr);
    for (auto i: arr)
        fprintf(stdout, "%d ", i);

    cout << endl;
    fclose(stdin);
    fclose(stdout);
    return 0;
}

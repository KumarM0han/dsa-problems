#include <iostream>
using namespace std;

// Definition for singly-linked list.
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution_My {
public:
    ListNode* find_common(ListNode* longer, ListNode* smaller, 
                          int sizeL, int sizeS) {
        int skip = sizeL-sizeS;
        ListNode* balance = longer;
        for (int i=0; i<skip; i++){
            balance = balance->next;
        }
        
        ListNode* moveL = balance;
        ListNode* moveS = smaller;
        while (moveL!=NULL && moveS!=NULL) {
            if (moveL == moveS){
                return moveL;
            }
            moveL = moveL->next;
            moveS = moveS->next;
        }
        return NULL;
    }
        
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
        int sizeA =0, sizeB =0;
        
        ListNode* tmp = headA;
        while (tmp!=NULL) {
            sizeA++;
            tmp = tmp->next;
        }
        tmp = headB;
        while (tmp!=NULL) {
            sizeB++;
            tmp = tmp->next;
        }
        
        int skip = sizeB-sizeA;
        ListNode* common;
        
        if (skip>=0) {
            common = find_common(headB, headA, sizeB, sizeA);
        } else {
            common = find_common(headA, headB, sizeA, sizeB);
        }
        return common;
    }
};

class Solution_Nick_White {
public:
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
        ListNode* a = headA;
        ListNode* b = headB;
        while (a!=b) {
            if (a==NULL) {
                a = headB;
            } else {
                a = a->next;
            }
            if (b==NULL) {
                b = headA;
            } else {
                b = b->next;
            }
        }
        
        return a;
    }
};